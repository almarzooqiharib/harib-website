#### Project Sketch
##### Light was always my passion I had to create something awesome something unique. The market is full of LED but not the same as the one I came up with. below is me brain storming!:
### Brain-storming for a minute I came up with four ideas:
1. Electric motor on wheels.
2. Baby bed that moves if the baby cried.
3. CircoLED
4. A multi function heat gun.
#### Processing through these four ideas. I settled with CircoLED, but why:
1. It's not available yet as a product.
2. It's programable.
3. Everybody needs light, and I need one.
## CircoLED
![](img/Final-Project/IMG1.JPG)
###### Colorful Circular LEDs with a diameter of 40-60 mm. Connects to each other like Beehive.

#### CircoLED Components:
###### Main LED
1. Power input
2. USB input
3. AUX input
4. Sensor input
5. RGBW output
6. Speaker output

###### Sub LED
1. RGBW
2. 8 power data slots
3. Speaker
4. Sensor
![](img/Final-Project/IMG2.JPG)
#### CircoLED Properties:
* Accessible by hard button and soft app.
Programable.
* Ability to change burned LEDs.
* Contains Speaker.
* Contains capacitive sensor(able to turn single LED off)
* Ability to attach as many CircoLED as you desire.
* Eight PD slots(limitless designs)

###### Many options are available as a product but none of which that looks like mine.
