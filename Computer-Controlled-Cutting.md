## Laser Cutting

Machine specification: Universal Laser System PLS6MW (Multi-Wavelength) is a
free-standing platform uses multiple laser wavelengths to process the broadest possible spectrum of
materials and supports either CO2 or fiber lasers.

### Cutting steps:
1. First step is turning the machine ON
![](img/week#5/IMG_3597.jpg)
![](img/week#5/IMG_3598.jpg)
2. Open the sketch in CoralDRAW and press (cmd P) to
initiate the printing process then click on Preferences.
![](img/week#5/IMG_3603.jpg)
![](img/week#5/IMG_3604.jpg)
3. Measure with the caliper the Material Thickness, Choose the
type of material (cast Acrylic) and put it on the work surface area.
![](img/week#5/IMG_3605.jpg)
4. On the Manual Control > laser Setting is sit to default (In most cases the default material settings will be sufficient and it will
not be necessary to adjust intensities), press Ok > Print.
![](img/week#5/IMG_3608.jpg)
![](img/week#5/IMG_3609.jpg)
![](img/week#5/IMG_3610.jpg)
5. Move on to Universal Laser System Control Panel Press Focus View (automatically the machine will get in focus)> Relocates View > To Pointer.
![](img/week#5/IMG_3615.jpg)
6. Start the vacuum.
7. Press Play.
8. Keep an eye and don't leave the machine running
9. Turn the vacuum off

## Vinyl Cutting
